# flightscopeaviation.com.au

## Project setup
```
./bin/deps.sh
npm install
```

### Compiles and hot-reloads for development
```
npm run serve
```

### Compiles and minifies for production
```
./bin/build.sh
```

### Lints and fixes files
```
npm run lint
```

### Creating a news article
1. Write your markdown in ./src/views/news/ARTICLE.md
2. Make sure to add

        <style lang="scss" scoped>
        @import '@/styles/_presets.scss';
        @include news-page;
        </style>
    at the bottom
3. Add the id (same as ARTICLE filename), title, description (shown on News list), and date (shown on News list) to ./src/views/news/news.js
