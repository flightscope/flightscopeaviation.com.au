#!/bin/sh

npm ci
npm run build
mv public public-vue # GitLab Pages hooks on the public folder
mv dist public # rename the dist folder (result of npm run build)
cp public/index.html public/404.html # move index.html to 404.html to provide history-mode
