#!/bin/sh

set -e

if ! type -p wget >/dev/null ; then
  >&2 echo "Missing: wget" >&2
fi

aircraft_images="https://flightscope.gitlab.io/aircraft/images"
aircraft_manual="https://flightscope.gitlab.io/aircraft/manual"
raaus_documentation="https://flightscope.gitlab.io/raaus-documentation"
radio_reference="https://flightscope.gitlab.io/radio-reference"
student_homework_sheets="https://flightscope.gitlab.io/training/student-homework-sheets"
archerfield="https://flightscope.gitlab.io/training/archerfield"
team="https://flightscope.gitlab.io/images/staff"
logo="https://flightscope.gitlab.io/images/logos"
www="https://flightscope.gitlab.io/images/www"

assets_deps="src/assets/images"

alias get='wget -q -b -N'

aircraft_images_list="\
  24-4844/exterior-front-600.jpg\
  24-4844/exterior-600.jpg\
  24-4844/cockpit-gps-close-up-600.jpg\
  24-4844/cockpit-600.jpg\
  24-4844/cockpit-pilot-side-600.jpg\
  24-4844/cockpit-trim-flaps-600.jpg\
  24-4844/cockpit-gps-600.jpg\
  24-4844/cockpit-pax-side-600.jpg\
  24-4844/cockpit-rudder-pedals-600.jpg\
  24-4844/4844-ident-600.jpg\
  24-5350/eurofox5350-2.jpg\
  24-5350/exterior-600.jpg\
  24-5350/cockpit-gps-close-up-600.jpg\
  24-5350/cockpit-gps-600.jpg\
  24-5350/cockpit-600.jpg\
  24-5350/cockpit-pax-side-600.jpg\
  24-5350/cockpit-pilot-side-600.jpg\
  24-5350/cockpit-rudder-pedals-600.jpg\
  24-5350/cockpit-trim-flaps-600.jpg\
  24-5350/exterior-600.jpg\
  24-5350/5350-ident-600.jpg\
  vh-bne/bne-exterior-600.jpg\
  vh-bne/aquila-a210.webp\
  vh-bne/cockpit-600.jpg\
  vh-bne/comm-stack-600.jpg\
  vh-bne/g500-600.jpg\
  vh-bne/throttle-600.jpg\
  vh-bne/bne-behind.jpg\
  vh-ciz/extra-300.webp\
  vh-ciz/BD5C3347-600.jpg\
  vh-ciz/BD5C3350-600.jpg\
  vh-ciz/BD5C3375-600.jpg\
  vh-ciz/BD5C3382-600.jpg\
  vh-ciz/BD5C3413-600.jpg\
  vh-ciz/BD5C3473-600.jpg\
  vh-ciz/BD5C3500-600.jpg\
  vh-ciz/BD5C3583-600.jpg\
  vh-ciz/BD5C3606-600.jpg\
  vh-ciz/BD5C3625-600.jpg\
  vh-ciz/BD5C3626-600.jpg\
  vh-ciz/BD5C3627-600.jpg\
  vh-ciz/BD5C3642-600.jpg\
  vh-ciz/BD5C3642-cropped.jpg\
  vh-ciz/BD5C3737-600.jpg\
  vh-ciz/BD5C3744-600.jpg\
  vh-ciz/BD5C3847-600.jpg\
  vh-ciz/extra-300-2-600.jpg\
  vh-scn/vh-scn-600.jpg\
  vh-scn/scn-exterior-600.jpg\
  vh-scn/attitude-indicator-600.jpg\
  vh-scn/cockpit-03-600.jpg\
  vh-scn/cockpit-06-600.jpg\
  vh-scn/cockpit-600.jpg\
  vh-scn/fuel-instruments-600.jpg\
  vh-scn/scn-exterior-600.jpg\
  vh-scn/cockpit-01-600.jpg\
  vh-scn/cockpit-04-600.jpg\
  vh-scn/cockpit-07-600.jpg\
  vh-scn/comm-stack-600.jpg\
  vh-scn/fuel-tank-instruments-600.jpg\
  vh-scn/switches-and-circuit-breakers-600.jpg\
  vh-scn/cockpit-02-600.jpg\
  vh-scn/cockpit-05-600.jpg\
  vh-scn/cockpit-08-600.jpg\
  vh-scn/flap-lever-600.jpg\
  vh-scn/pilot-side-600.jpg\
  vh-scn/throttle-mixture-control-600.jpg\
  vh-nqx/cockpit-breakers-fuel-600.jpg\
  vh-nqx/cockpit-600.jpg\
  vh-nqx/cockpit-left-600.jpg\
  vh-nqx/cockpit-right-bottom2-600.jpg\
  vh-nqx/cockpit-throttle-600.jpg\
  vh-nqx/left-behind-600.jpg\
  vh-nqx/right-behind2-600.jpg\
  vh-nqx/cockpit-centre-600.jpg\
  vh-nqx/cockpit-left-bottom2-600.jpg\
  vh-nqx/cockpit-left-seat-600.jpg\
  vh-nqx/cockpit-right-bottom-600.jpg\
  vh-nqx/front-600.jpg\
  vh-nqx/left-front-600.jpg\
  vh-nqx/right-behind-600.jpg\
  vh-nqx/cockpit-fuel-600.jpg\
  vh-nqx/cockpit-left-bottom-600.jpg\
  vh-nqx/cockpit-rear-seat-600.jpg\
  vh-nqx/cockpit-right-600.jpg\
  vh-nqx/left-behind2-600.jpg\
  vh-nqx/left-lateral-600.jpg\
  vh-nqx/right-lateral-600.jpg\
  "

aircraft_manual_list="\
  8kcab/vh-nqx/8kcab_vh-nqx_page1.pdf-150.png\
  aquila-a210/FM_AT01_1010_100E_B04_page1.pdf-150.png\
  aquila-a210/SB_AT01_029_02-AFM_page_1-8_2-4_5_page1.pdf-150.png\
  aquila-a210/vh-bne/aquila-a210_vh-bne_page1.pdf-150.png\
  cessna-152/vh-egy/00-certificate-of-registration_page1.pdf-150.png\
  cessna-152/vh-egy/01-cover_page1.pdf-150.png\
  cessna-152/vh-egy/02-performance-specifications_page1.pdf-150.png\
  cessna-152/vh-egy/03-table-of-contents_page1.pdf-150.png\
  cessna-152/vh-egy/04-section1-general_page1.pdf-150.png\
  cessna-152/vh-egy/05-section2-limitations_page1.pdf-150.png\
  cessna-152/vh-egy/06-section3-emergency-procedures_page1.pdf-150.png\
  cessna-152/vh-egy/07-section4-normal-procedures_page1.pdf-150.png\
  cessna-152/vh-egy/08-stc_SA1000NW_page1.pdf-150.png\
  cessna-152/vh-egy/09-section5-performance_page1.pdf-150.png\
  cessna-152/vh-egy/10-section6-weight-and-balance_page1.pdf-150.png\
  cessna-152/vh-egy/11-section7-airplane-and-system-descriptions_page1.pdf-150.png\
  cessna-152/vh-egy/12-section8-airplane-handling-and-maintenance_page1.pdf-150.png\
  cessna-152/vh-egy/13-section9-supplements_page1.pdf-150.png\
  cessna-172n/Cessna_172_C172N-1978-POH-S1to7-scanned_page1.pdf-150.png\
  cessna-172r/skyhawk_pim-20130523_page1.pdf-150.png\
  cessna-172r/vh-scn/cessna-172r_vh-scn_page1.pdf-150.png\
  eurofox-3k/24-4844/eurofox-3k_24-4844_page1.pdf-150.png\
  eurofox-3k/24-5350/eurofox-3k_24-5350_page1.pdf-150.png\
  eurofox-3k/a240-poh_page1.pdf-150.png\
  eurofox-3k/Annex_01-151107_page1.pdf-150.png\
  eurofox-3k/maintenance-manual_page1.pdf-150.png\
  eurofox-3k/POH_Supplement_MTOW_page1.pdf-150.png\
  rotax-912/OM_912_Series_ED4_R0_page1.pdf-150.png\
  "

raaus_documentation_list="\
  1-syllabus-of-flight-training-issue-7-v2-single-pages-1/1.01-group-a-3-axis-syllabus_page1.pdf-150.png\
  1-syllabus-of-flight-training-issue-7-v2-single-pages-1/1.04-passenger-carrying-endorsement-syllabus_page1.pdf-150.png\
  1-syllabus-of-flight-training-issue-7-v2-single-pages-1/1.05-cross-country-endorsement-syllabus_page1.pdf-150.png\
  1-syllabus-of-flight-training-issue-7-v2-single-pages-1/1.06-formation-endorsement-syllabus_page1.pdf-150.png\
  1-syllabus-of-flight-training-issue-7-v2-single-pages-1/1.08-low-level-endorsement-syllabus_page1.pdf-150.png\
  1-syllabus-of-flight-training-issue-7-v2-single-pages-1/2.01-basic-aeronautical-knowledge-syllabus_page1.pdf-150.png\
  1-syllabus-of-flight-training-issue-7-v2-single-pages-1/2.02-air-legislation-syllabus_page1.pdf-150.png\
  1-syllabus-of-flight-training-issue-7-v2-single-pages-1/2.03-navigation-and-meteorology-syllabus_page1.pdf-150.png\
  1-syllabus-of-flight-training-issue-7-v2-single-pages-1/2.04-radio-operator-syllabus_page1.pdf-150.png\
  1-syllabus-of-flight-training-issue-7-v2-single-pages-1/2.05-human-factors-syllabus_page1.pdf-150.png\
  1-syllabus-of-flight-training-issue-7-v2-single-pages-1_page1.pdf-150.png\
  1-syllabus-of-flight-training-issue-7-v2-single-pages-1/table-of-contents_page1.pdf-150.png\
  2-raaus-technical-manual-issue-4-single-pages/12.1-daily-and-pre-flight-inspections_page1.pdf-150.png\
  2-raaus-technical-manual-issue-4-single-pages/5.1-aircraft-registration_page1.pdf-150.png\
  2-raaus-technical-manual-issue-4-single-pages/abbreviations-and-definitions_page1.pdf-150.png\
  2-raaus-technical-manual-issue-4-single-pages_page1.pdf-150.png\
  2-raaus-technical-manual-issue-4-single-pages/table-of-contents_page1.pdf-150.png\
  5-om-71-august-2016-single-pages/2.01-pilot-certificate-group-a-and-b_page1.pdf-150.png\
  5-om-71-august-2016-single-pages/2.04-pilot-certificates-aeroplane-groups_page1.pdf-150.png\
  5-om-71-august-2016-single-pages/2.06-student-or-converting-pilot-certificate_page1.pdf-150.png\
  5-om-71-august-2016-single-pages/2.07-pilot-certificate-group-a-and-b_page1.pdf-150.png\
  5-om-71-august-2016-single-pages_page1.pdf-150.png\
  5-om-71-august-2016-single-pages/table-of-contents_page1.pdf-150.png\
  "

radio_reference_list="\
  ybaf_after-start-up_page1.pdf-150.png\
  ybaf_holding-ready_page1.pdf-150.png\
  ybaf_target-inbound_page1.pdf-150.png\
  "

student_homework_sheets_list="\
  effect-of-controls_page1.pdf-150.png\
  straight-and-level_page1.pdf-150.png\
  climbing-and-descending_page1.pdf-150.png\
  turning_page1.pdf-150.png\
  circuits-crosswind_page1.pdf-150.png\
  stalling_page1.pdf-150.png\
  circuits_page1.pdf-150.png\
  circuit-emergencies_page1.pdf-150.png\
  circuits-crosswind_page1.pdf-150.png\
  forced-landings_page1.pdf-150.png\
  precautionary-search-and-landing_page1.pdf-150.png\
  steep-turns_page1.pdf-150.png\
  maintenance-release_page1.pdf-150.png\
  archerfield-operations_page1.pdf-150.png\
  area-solo_page1.pdf-150.png\
  rpc-exam-preparation_page1.pdf-150.png\
  pax-endorsement_page1.pdf-150.png\
  nav-preparation_page1.pdf-150.png\
  whiz-wheel-exercises_page1.pdf-150.png\
  "

archerfield_list="\
  16-106FAC_Tips-for-flying-at-Archerfield_WEB_page1-150.png\
  archerfield-visual-pilot-guide-2010_page1-150.png\
  Brisbane_Sunshine_VTC_rotateW-150.png\
  Brisbane_VNC-150.png\
  ersa-FAC_YBAF_page1-150.png\
  ersa-RDS_YBAF_page1-150.png\
  "

team_list="\
  craig-lathwood-250.png\
  jimmy-serrano-cropped-250.jpg\
  nathan-holloway-cropped-250.jpg\
  regan-lacey1-cropped-250.jpg\
  rod-flockhart-250.png\
  steve-taddeucci-cropped-250.jpg\
  tom-evans-250.png\
  tony-morris-cropped-250.jpg\
  "

logo_list="\
  logo-straight-800.png\
  "

www_list="\
  front1-800.jpg\
  classroom-anime-100.png\
  flightscope-location-450.png\
  videos-resource-250.png\
  home-parallax.jpg\
  kieran-brown-flying-scholarship-800.png\
  contact-800.jpg\
  moreabout-800.png\
  arrow-800.png\
  cards/1-1400.jpg\
  cards/2-1400.jpg\
  cards/3-1400.jpg\
  cards/4-1400.jpg\
  cards/5-1400.jpg\
  cards/6-1400.jpg\
  cards/7-1400.jpg\
  cards/8-1400.jpg\
  cards/9-1400.jpg\
  cards/10-1400.jpg\
  cards/11-1400.jpg\
  cards/12-1400.jpg\
  programs/ra-aus-flight-instructor-course-800.png\
  programs/ra-aus-navigation-endorsement-800.jpg\
  programs/recreational-pilot-certificate-800.png\
  programs/recreational-pilot-licence-800.png\
  slideshow/home_slideshow_1-1400.jpg\
  slideshow/home_slideshow_2-1400.jpg\
  slideshow/home_slideshow_3-1400.jpg\
  slideshow/home_slideshow_4-1400.jpg\
  bne-ciz-ysa-formation.jpg\
  "

for img in ${aircraft_images_list}
do
  dn=`dirname $img`
  mkdir -p $assets_deps/aircraft_images/$dn
  out=$assets_deps/aircraft_images/$dn
  get -P $out $aircraft_images/$img
done

for img in ${aircraft_manual_list}
do
  dn=`dirname $img`
  mkdir -p $assets_deps/aircraft_manual/$dn
  out=$assets_deps/aircraft_manual/$dn
  get -P $out $aircraft_manual/$img
done

for img in ${raaus_documentation_list}
do
  dn=`dirname $img`
  mkdir -p $assets_deps/raaus_documentation/$dn
  out=$assets_deps/raaus_documentation/$dn
  get -P $out $raaus_documentation/$img
done

for img in ${radio_reference_list}
do
  dn=`dirname $img`
  mkdir -p $assets_deps/radio_reference/$dn
  out=$assets_deps/radio_reference/$dn
  get -P $out $radio_reference/$img
done

for img in ${student_homework_sheets_list}
do
  dn=`dirname $img`
  mkdir -p $assets_deps/student_homework_sheets/$dn
  out=$assets_deps/student_homework_sheets/$dn
  get -P $out $student_homework_sheets/$img
done

for img in ${archerfield_list}
do
  dn=`dirname $img`
  mkdir -p $assets_deps/archerfield/$dn
  out=$assets_deps/archerfield/$dn
  get -P $out $archerfield/$img
done

for img in ${team_list}
do
  dn=`dirname $img`
  mkdir -p $assets_deps/team/$dn
  out=$assets_deps/team/$dn
  get -P $out $team/$img
done

for img in ${logo_list}
do
  dn=`dirname $img`
  mkdir -p $assets_deps/logo/$dn
  out=$assets_deps/logo/$dn
  get -P $out $logo/$img
done

for img in ${www_list}
do
  dn=`dirname $img`
  mkdir -p $assets_deps/www/$dn
  out=$assets_deps/www/$dn
  get -P $out $www/$img
done
