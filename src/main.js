import Vue from 'vue'
import App from './App.vue'
import router from './router'
import '@splidejs/splide/dist/css/themes/splide-default.min.css'
import { FontAwesomeIcon } from '@fortawesome/vue-fontawesome'
import { library } from '@fortawesome/fontawesome-svg-core'
import { faFacebookF, faInstagram, faTwitter, faYoutube } from '@fortawesome/free-brands-svg-icons'
import { faMapMarkerAlt, faBars, faCaretRight, faFileAlt, faBroadcastTower, faPencilRuler, faVideo, faFile, faFileSignature, faCaretDown } from '@fortawesome/free-solid-svg-icons'

library.add(faFacebookF, faMapMarkerAlt, faTwitter, faInstagram, faYoutube, faBars, faCaretRight, faFileAlt, faBroadcastTower, faPencilRuler, faVideo, faFile, faFileSignature, faCaretDown)
Vue.component('font-awesome-icon', FontAwesomeIcon)

Vue.config.productionTip = false

new Vue({
  router,
  render: h => h(App)
}).$mount('#app')
