import Vue from 'vue'
import VueRouter from 'vue-router'
import news from '@/views/news/news.js'

Vue.use(VueRouter)

const normalRoutes = [
  {
    path: '/',
    component: () => import('@/views/Home.vue')
  },
  {
    path: '/contact',
    meta: {
      title: 'Contact'
    },
    component: () => import('@/views/Contact.vue')
  },
  {
    path: '/recreational-pilot-certificate',
    meta: {
      title: 'Recreation Pilot Certificate'
    },
    component: () => import('@/views/programs/RecreationalPilotCertificate.vue')
  },
  {
    path: '/recreational-pilot-licence',
    meta: {
      title: 'Recreational Pilot Licence'
    },
    component: () => import('@/views/programs/RecreationalPilotLicence.vue')
  },
  {
    path: '/ra-aus-navigation-endorsement',
    meta: {
      title: 'RA-Aus Navigation Endorsement'
    },
    component: () => import('@/views/programs/RAAusNavigationEndorsement.vue')
  },
  {
    path: '/private-pilot-licence',
    meta: {
      title: 'Private Pilot Licence'
    },
    component: () => import('@/views/programs/PrivatePilotLicence.vue')
  },
  {
    path: '/ra-aus-flight-instructor-course',
    meta: {
      title: 'RA-Aus Flight Instructor Course'
    },
    component: () => import('@/views/programs/RAAusFlightInstructorCourse.vue')
  },
  {
    path: '/ra-aus-passenger-carrying-endorsement',
    meta: {
      title: 'RA-Aus Passenger Carrying Endorsement'
    },
    component: () => import('@/views/programs/RAAusPassengerCarryingEndorsement.vue')
  },
  {
    path: '/commercial-pilot-licence',
    meta: {
      title: 'Commercial Pilot Licence'
    },
    component: () => import('@/views/programs/CommercialPilotLicence.vue')
  },
  {
    path: '/rpc-to-rpl-conversion',
    meta: {
      title: 'RPC to RPL Conversion'
    },
    component: () => import('@/views/programs/RPCToRPLConversion.vue')
  },
  {
    path: '/uprt-upset-prevention-and-recovery-training',
    meta: {
      title: 'UPRT - Upset Prevention and Recovery Training'
    },
    component: () => import('@/views/programs/UPRT.vue')
  },
  {
    path: '/tailwheel-endorsement',
    meta: {
      title: 'Tailwheel Endorsement'
    },
    component: () => import('@/views/programs/TailwheelEndorsement.vue')
  },
  {
    path: '/formation-flying',
    meta: {
      title: 'Formation Flying'
    },
    component: () => import('@/views/programs/FormationFlying.vue')
  },
  {
    path: '/aerobatic-endorsement',
    meta: {
      title: 'Aerobatic Endorsement'
    },
    component: () => import('@/views/programs/AerobaticEndorsement.vue')
  },
  {
    path: '/about-us',
    meta: {
      title: 'About Us'
    },
    component: () => import('@/views/AboutUs.vue')
  },
  {
    path: '/faq',
    meta: {
      title: 'FAQ'
    },
    component: () => import('@/views/FAQ.vue')
  },
  {
    path: '/location',
    meta: {
      title: 'Location'
    },
    component: () => import('@/views/Location.vue')
  },
  {
    path: '/programs',
    meta: {
      title: 'Programs'
    },
    component: () => import('@/views/Programs.vue')
  },
  {
    path: '/student-resources',
    meta: {
      title: 'Student Resources'
    },
    component: () => import('@/views/StudentResources.vue')
  },
  {
    path: '/aircraft-documentation',
    meta: {
      title: 'Aircraft Documentation'
    },
    component: () => import('@/views/student-resources/AircraftDocumentation.vue')
  },
  {
    path: '/archerfield-documentation',
    meta: {
      title: 'Archerfield Documentation'
    },
    component: () => import('@/views/student-resources/ArcherfieldDocumentation.vue')
  },
  {
    path: '/radio-practice',
    meta: {
      title: 'Radio Practice'
    },
    component: () => import('@/views/student-resources/RadioPractice.vue')
  },
  {
    path: '/videos',
    meta: {
      title: 'Videos'
    },
    component: () => import('@/views/student-resources/Videos.vue')
  },
  {
    path: '/practice-drills',
    meta: {
      title: 'Practice Drills'
    },
    component: () => import('@/views/student-resources/PracticeDrills.vue')
  },
  {
    path: '/navigation-1-in-60-rule',
    meta: {
      title: 'Navigation 1 in 60 Rule'
    },
    component: () => import('@/views/student-resources/practice-drills/Navigation1in60Rule.vue')
  },
  {
    path: '/pressure-altitude-from-qnh-and-elevation',
    meta: {
      title: 'Pressure Altitude from QNH and Elevation'
    },
    component: () => import('@/views/student-resources/practice-drills/PressureAltitudeFromQNHAndElevation.vue')
  },
  {
    path: '/density-altitude-from-pressure-altitude-and-temperature',
    meta: {
      title: 'Density Altitude from Pressure Altitude and Temperature'
    },
    component: () => import('@/views/student-resources/practice-drills/DensityAltitudeFromPressureAltitudeAndTemperature.vue')
  },
  {
    path: '/ra-aus-documentation',
    meta: {
      title: 'RA-Aus Documentation'
    },
    component: () => import('@/views/student-resources/RAAusDocumentation.vue')
  },
  {
    path: '/homework-sheets',
    meta: {
      title: 'Homework Sheets'
    },
    component: () => import('@/views/student-resources/HomeworkSheets.vue')
  },
  {
    path: '/fleet',
    meta: {
      title: 'Fleet'
    },
    component: () => import('@/views/Fleet.vue')
  },
  {
    path: '/news',
    meta: {
      title: 'News'
    },
    component: () => import('@/views/News.vue')
  },
  {
    path: '*',
    meta: {
      title: 'Page Not Found'
    },
    component: () => import('@/views/PageNotFound.vue')
  }
]

const newsRoutes = news.map(entry => ({
  path: '/' + entry.id,
  meta: {
    title: entry.title
  },
  component: () => import('@/views/news/' + entry.id + '.md')
}))

const router = new VueRouter({
  mode: 'history',
  base: process.env.BASE_URL,
  routes: [
    ...normalRoutes,
    ...newsRoutes
  ],
  scrollBehavior (to, from, savedPosition) {
    return { x: 0, y: 0 }
  }
})

export default router
