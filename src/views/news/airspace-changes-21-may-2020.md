# Airspace Changes 21 May 2020
From 21 May 2020, the designated airspace around Archerfield and Sunshine Coast airports will be undergoing a number of changes Please view the following animations, excerpted from the Brisbane/Sunshine Coast Visual Terminal Charts, to see the differences.

[Sunshine Coast](https://flightscope.gitlab.io/airspace-changes-2020/loop/ybsu_07NOV2019-21MAY2020.gif)

[Archerfield](https://flightscope.gitlab.io/airspace-changes-2020/loop/ybaf_07NOV2019-21MAY2020.gif)

<style lang="scss" scoped>
@import '@/styles/_presets.scss';
@include news-page;
</style>

