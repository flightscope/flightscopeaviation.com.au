# Increased Traffic Alerts 28 Jan 2021

As you might be aware, on an eastern departure from Archerfield, we change the radio frequency to Brisbane Centre 125.7MHz and transponder to 1200, once outside the Archerfield Control Zone.

Recently, our instructors have been receiving an increasing number of traffic alerts from Brisbane Centre in the area between Target and Mt Cotton, while in Class G (uncontrolled) airspace.

To give an example, you might hear the following on the centre frequecy

> VFR aircraft, two miles east of Target, tracking east, one thousand five hundred, Brisbane Centre, you have traffic three o'clock, one thousand six hundred, tracking north, converging

If you believe you are that aircraft, or perhaps the traffic being reported, make an attempt to visually identify the reported traffic and when it is safe to do so *(priority 1 is eyes outside, fly the aeroplane)*, provide an accurate position report to Brisbane Centre:

> Brisbane Centre, Eurofox 5350, two miles east of Target, maintaining one thousand five hundred, tracking east

Brisbane Centre is then likely to ask you to squawk **IDENT**:

> Eurofox 5350, squawk IDENT

Respond with acknowledgement:

> Eurofox 5350

Next, press the **IDENT** button on the transponder, and confirm this by an indication on the transponder display that it is now squawking IDENT. The IDENT button for both our Eurofox aircraft can be seen in these images:

----

*How do I embed images in a news article?*

https://gitlab.com/flightscope/aircraft/images/24-4844/-/blob/master/src/4844-ident.jpg

https://gitlab.com/flightscope/aircraft/images/24-5350/-/blob/master/src/5350-ident.jpg

----

Using the IDENT button when requested helps Brisbane Centre know which aircraft is you on their radar display. Centre may continue providing you with traffic reports in the area until the traffic conflict is over, and you would keep them updated with any traffic that you have in sight, your intentions or any other important information to the situation.

Remember: **Aviate, Navigate, Communicate**

Happy flying!

<style lang="scss" scoped>
@import '@/styles/_presets.scss';
@include news-page;
</style>

