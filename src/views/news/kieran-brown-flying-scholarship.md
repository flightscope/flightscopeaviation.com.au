# Kieran Brown Flying Scholarship
## Click [here](https://www.kieranbrownflyingscholarship.com/) to visit the official page.

### The Man Himself
<img src="@/assets/images/www/kieran-brown-flying-scholarship-800.png" />

Kieran was born 13 May 1973 and left this world, without notice on the 20 March 2019, doing what he loved best.

Kieran had a great love of aviation and his passion for 'anything that flew' was always evident. In 1989 he learnt to fly at Camden in NSW at the age of 16 and was the youngest intake for an Engineering Apprenticeship with Hawker De Havilland at Bankstown Airport in the same year.

Kieran had vast industry experience as a Licenced Aircraft Engineer and Commercial Pilot in both aeroplanes and helicopters. He devoted his life to all things aviation and could fly literally anything!

He worked as a Maintenance Engineer with Helimuster, Victoria River Downs Station, NT, Westpac Rescue Helicopters, Newcastle and Aeropower, Parafield, SA, as a Line Pilot for HAWCS, QLD, Aeropower, Redcliffe, QLD, and a Ground Operations Manager, Pacific Helicopters, Goroka, PNG.

His love for flying helicopters saw his return to Aeropower, Redcliffe, QLD in 2018 as their Deputy Chief Pilot.

To ensure Kieran's legacy lives on Kieran's mother, Bev Jeffery in conjunction with Rod Flockhart of Flightscope Aviation would like to offer an annual Memorial Scholarship in Kieran's name.

The winner of the inaugural Kieran Brown Memorial flying Scholarship will undertake training for their RA-Aus Pilot Certificate with Flightscope Aviation at Archerfield Airport, QLD. 

### Information
The Kieran Brown Memorial Flying Scholarship is organised by Kieran's Mum, Beverley Jeffery in conjunction with Flightscope Aviation's Director, Rodney Flockhart.

The scholarship is for training purposes for a RA-Aus Pilot Certificate to the value of $10,000.00.

Entrants will be assessed on their suitability for formal flying training.

Beverley Jeffery and Rodney Flockhart will select THREE finalists to be entered in the draw for the prize. Each finalist will be notified in writing no later than 20 January 2020. Finalists will be given a FREE Trial Introductory Flight with Flightscope Aviation, valued at $295. 

### Applications
* Open: 20 August 2019
* Close: 20 December 2019
* Finalists notified: 20 January 2020
* Winner notified: 20 February 2020

To apply, simply send a short video of yourself and why you want to learn to fly.
Applications to be sent to [kieranbrownflyingscholarship@gmail.com](mailto:kieranbrownflyingscholarship@gmail.com) or

Ms B Jeffery,
PO Box 1129,
MULLUMBIMBY NSW 2482.

### To qualify, applicants must: 
1. Be at least 15 years of age on 20 December 2019
2. Give an undertaking that they have had no formal flying training,
3. Submit a short video clip depicting who they are and why they want to learn to fly,
4. Be able to meet Recreational or Private Pilot Licence Medical Standards,
5. Be an Australian Citizen.

### Terms and conditions
1. The Kieran Brown Memorial Flying Scholarship must be flown with Flightscope Aviation at Archerfield Airport, QLD, Australia.
2. The Kieran Brown Memorial Flying Scholarship must be expended by 20 March 2021.
3. The value of the Kieran Brown Memorial Flying Scholarship is non-transferable.
4. Employees of Flightscope Aviation and their immediate families are not eligible for the Scholarship.
5. Immediate family members of Kieran Leslie Brown are not eligible for the Scholarship.
6. Beverley Jeffery, Rodney Flockhart and Flightscope Aviation and its employees accept no responsibility or liability for any act or incident that occurs in connection with this scholarship which results from or is caused by any act or omission of the organisers or any other person.

<style lang="scss" scoped>
@import '@/styles/_presets.scss';
@include news-page;
</style>
