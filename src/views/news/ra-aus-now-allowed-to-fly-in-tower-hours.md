# RA-AUS - NOW ALLOWED TO FLY IN TOWER HOURS
## Get a CASA-issued medical and fly when you like.
We are now allowed to send our RA Aus students (and RPC holders) solo during tower hours here at Archerfield. YIPPEE!!! No more early mornings and late nights.

All you need to do is:
- Complete the RA Aus Radio Exam
- Get an ARN
- Hold a CASA-issued Medical

The CASA issued medical can be any level from their "Recreational Aviation Medical Practitioner's Certificate (RAMPC)" to "Class 1".

If you are looking to progress into GA, we would recommend getting your CASA Class 2 Medical (PPL) and Class 1 Medical (CPL).

Click the button below to start your CASA Medical Application or ARN Application process.

[CASA Medical Application](https://mrs.casa.gov.au/Applicant)

[ARN Application](https://www.casa.gov.au/licences-and-certification/individual-licensing/aviation-reference-numbers)

<style lang="scss" scoped>
@import '@/styles/_presets.scss';
@include news-page;
</style>
